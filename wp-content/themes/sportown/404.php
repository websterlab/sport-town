<? get_header(); ?>

<div class="container">
  <div class="row">
    <div class="col-12 my-5 text-center">
      <div class="error-404">404</div>
    </div>
    <div class="col-12 text-center mb-5">
      <a href="/" class="btn-blue">Вернуться главную</a>
    </div>
  </div>
</div>

<? get_footer(); ?>
