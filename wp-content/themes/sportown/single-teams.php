<?php get_header(); ?>

<? get_template_part('tpl/breadcrumbs'); ?>

<div class="">
  <div id="modal-ready">
    <div class="container my-5">
      <div class="row inner-page">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="col-lg-4 teams">
              <div class="teams-image">
                <? $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb'); ?>
                <img src="<?=$thumbnail_attributes[0];?>" class="img-fluid">
              </div>
              <div class="name"><? the_title(); ?></div>
              <div class="master"><? the_field('sub_title', get_the_ID()); ?></div>
              <p class="stage">Тренерский стаж - <? the_field('coaching_staff', get_the_ID()); ?></p>
            </div>
            <div class="col-lg-8">
              <article>
                <? the_content(); ?>
              </article>
            </div>
          <? endwhile; else: ?>
            <h1>Страница не найдена</h1>
          <? endif; ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
