<?
/*
    Template Name: Акция
    Template Post Type: page
*/

/**
 * Акция (promotion.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<? get_header(); ?>

<?
  //Текущие события
  $current_event = new WP_Query([
    'post_type' => 'promo',
    'posts_per_page' => '-1',
    'meta_query' => [
      'relation' => 'AND',
      [
        'key' => 'promo_date_end',
        'value' => date('Y-m-d'),
        'type' => 'DATE',
        'compare' => '>='
      ]
    ]
  ]);
?>

<div class="container mt-5">

  <div class="row d-block d-sm-none">
    <div class="col-12">
      <h1 class="page-title"><? the_title(); ?></h1>
    </div>
  </div>



  <? if($current_event->have_posts()): while($current_event->have_posts()): $current_event->the_post(); ?>
  <div class="row event-item">
    <? if(strlen(get_the_content())>260): ?>
    <a href="<? the_permalink(); ?>" class="stretched-link"></a>
    <? endif; ?>
    <div class="col-md-6 pr-5">
      <div class="event-date"><? the_field('promo_date_start'); ?> <span>&mdash;</span> <? the_field('promo_date_end'); ?></div>
      <div class="event-title"><? the_title(); ?></div>
      <div class="event-text"><?=get_the_excerpt(); ?></div>
    </div>
    <div class="col-md-6 event-img order-first order-sm-last">
      <? $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
      <img src="<?=$thumbnail_attributes[0];?>" class="img-fluid">
    </div>
  </div>
  <? wp_reset_query(); ?>
  <? endwhile; else: ?>
    <pre>Записей не найдено</pre>
  <? endif; ?>
</div>

<!-- <?
  //Прошедшие акции
  $last_event = new WP_Query([
    'post_type' => 'promo',
    'posts_per_page' => '-1',
    'meta_query' => [
      'relation' => 'AND',
      [
        'key' => 'promo_date_end',
        'value' => date('Y-m-d'),
        'type' => 'DATE',
        'compare' => '<='
      ]
    ]
  ]);
?> -->
<!-- <? if(count($last_event->posts)>0): ?>
<div class="container py-5">
  <div class="row">
    <div class="col-lg-3">
      <h2 class="event-archive">Архив акций</h2>
    </div>
    <div class="col-lg-9">
      <? if($last_event->have_posts()): while($last_event->have_posts()): $last_event->the_post(); ?>
      <div class="event-archive-item">
        <div class="event-date"><? the_field('promo_date_start'); ?> <span>&mdash;</span> <? the_field('promo_date_end'); ?></div>
        <div class="event"><? the_title(); ?></div>
      </div>
      <? wp_reset_query(); ?>
      <? endwhile; endif; ?>
      <a href="#" class="btn-b-blue mt-5">Показать ещё</a>
    </div>
  </div>
</div>
<? endif; ?> -->

<? get_footer(); ?>
