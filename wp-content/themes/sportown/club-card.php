<?
/*
    Template Name: Клубная карта
    Template Post Type: page
*/

/**
 * Клубная карта (club-card.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<?php get_header(); ?>

<div class="landing-block club-block-1">
  <div class="container">
    <div class="row">
      <div class="col-12 d-block d-sm-none">
        <h1 class="page-title"><? the_title(); ?></h1>
      </div>
      <div class="col">
        <div class="club-block-sub">Стартовый блок:</div>
        <ul class="sub-set">
          <li>Вводная тренировка в тренажерном зале</li>
          <li>Вводная тренировка  в бассейне</li>
          <li>Пробный массаж, консультация и экспрес-процедура у косметолога</li>
          <li>Вводная тренировка  на корте</li>
          <li>Фитнес-тестирование</li>
          <li>Вводная тренировка в групповых программах</li>
        </ul>
        <div class="club-block-line"></div>
        <div class="list-h">Взрослая</div>
        <ul class="sub-list">
          <li>Стартовый блок</li>
          <li>Свободное посещение всех зон клуба</li>
          <li>Доступ ко всем услугам клуба *</li>
        </ul>
        <a href="#" class="btn-b-green">Индвидуальный подбор карты</a>
        <div class="text">* Количество услуг в блоке зависит от вида клубного членства</div>
      </div>
    </div>
  </div>
</div>
<div class="landing-block club-block-2">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h">Юниор</div>
        <ul class="sub-list">
          <li>Стартовый блок</li>
          <li>Свободное посещение всех зон клуба</li>
          <li>Доступ ко всем услугам клуба *</li>
        </ul>
        <a href="#" class="btn-b-white">Индвидуальный подбор карты</a>
        <div class="text">* Количество услуг в блоке зависит от вида клубного членства</div>
      </div>
    </div>
  </div>
</div>
<div class="landing-block club-block-3">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h">Детская</div>
        <ul class="sub-list">
          <li>Занятия в бассеине на свежем воздухе</li>
          <li>Детский клуб</li>
          <li>Бассеин (только в присутствии тренера или&nbsp;родителей)</li>
        </ul>
        <a href="#" class="btn-b-white">Индвидуальный подбор карты</a>
      </div>
    </div>
  </div>
</div>
<div class="landing-block club-block-4">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h">Специальная</div>
        <ul class="sub-list">
          <li>Будущие мамы</li>
          <li>Элегантный возраст</li>
          <li>Студенты</li>
        </ul>
        <a href="#" class="btn-b-white">Индвидуальный подбор карты</a>
        <div class="text">* Количество услуг в блоке зависит от вида клубного членства</div>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
