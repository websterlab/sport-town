<?
/*
    Template Name: Контакты
    Template Post Type: page
*/

/**
 * Контакты (contacts.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<?php get_header(); ?>

<div class="container page-wrapper">
  <div class="row">

    <div class="col-12 d-none d-sm-block">
      <h1 class="page-title">Карта проезда</h1>
    </div>

    <div class="col-md-9">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2247.321065846711!2d37.88796441568332!3d55.71817230213018!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x414ac9f5c2f13373%3A0xf59fa7d8115ce5eb!2z0KTQuNGC0L3QtdGBLdC60LvRg9CxIFNQT1JUT1dOINCyINCa0L7QttGD0YXQvtCy0L4gfCDQkdCw0YHRgdC10LnQvSwg0YLRgNC10L3QsNC20LXRgNC90YvQuSDQt9Cw0LssINCz0YDRg9C_0L_QvtCy0YvQtSDQv9GA0L7Qs9GA0LDQvNC80Ys!5e0!3m2!1sru!2sru!4v1599048294504!5m2!1sru!2sru" width="100%" height="600" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

    <div class="col-md-3 order-first order-sm-last">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="inner-page">
          <div class="sidebar-title"><? the_title(); ?></div>
          <? the_content(); ?>
        </article>
      <? endwhile; else: ?>
        <h1>Страница не найдена</h1>
      <? endif; ?>
    </div>

  </div>

</div>



<?php get_footer(); ?>
