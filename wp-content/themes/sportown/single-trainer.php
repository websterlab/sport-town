<?php get_header(); ?>

<? get_template_part('tpl/breadcrumbs'); ?>

<div class="">
  <div id="modal-ready">
    <div class="container my-5">
      <div class="row inner-page">
          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="col-lg-4 teams">
              <div class="teams-image">
                <? $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb'); ?>
                <img src="<?=$thumbnail_attributes[0];?>" class="img-fluid">
              </div>
              <h3 class="name"><? the_title(); ?></h3>
              <p class="stage"><? the_field('model', get_the_ID()); ?></p>
            </div>
            <div class="col-lg-8">
              <article>
                <? the_content(); ?>
              </article>
            </div>
          <? endwhile; else: ?>
            <h1>Страница не найдена</h1>
          <? endif; ?>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
