<?php get_header(); ?>

<div class="container page-wrapper mt-5">
  <div class="row">

    <div class="col-lg-9">
      <div class="event-date"><? the_field('promo_date_start'); ?> <span>&mdash;</span> <? the_field('promo_date_end'); ?></div>
      <h1 class="page-title"><? the_title(); ?></h1>
      <? $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
      <img src="<?=$thumbnail_attributes[0];?>" class="img-fluid img-post">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="inner-page">
          <? the_content(); ?>
        </article>
      <? endwhile; else: ?>
        <h1>Страница не найдена</h1>
      <? endif; ?>
    </div>

    <div class="col-lg-3 d-none d-sm-block">
      <div class="sidebar text-center">
        <? get_template_part('tpl/sidebar-info'); ?>
      </div>
    </div>

  </div>
</div>

<?php get_footer(); ?>
