<?
/*
    Template Name: Пространство клуба (внутренняя)
    Template Post Type: page
*/

/**
 * Пространство клуба (внутренняя) (inner-page-space.php)
 * @package WordPress
 * @subpackage sportown
*/
?>
<?php get_header(); ?>

<? get_template_part('tpl/breadcrumbs'); ?>

<div class="container page-wrapper">
  <div class="row">

    <div class="col-12 d-none d-sm-block">
      <h1 class="page-title"><? the_title(); ?></h1>
    </div>

    <div class="col-lg-9">

      <!-- <div class="st-gallery">
        <div class="main-img"><a href="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="stretched-link"><img src="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="img-fluid"></a></div>
        <div class="add-img">
          <div class="add-img-item"><a href="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="stretched-link"><img src="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="img-fluid"></a></div>
          <div class="add-img-item"><a href="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="stretched-link"><img src="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="img-fluid"></a></div>
          <div class="add-img-item"><a href="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="stretched-link"><img src="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="img-fluid"></a></div>
          <div class="add-img-item"><a href="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="stretched-link"><img src="<?php bloginfo('template_url'); ?>/img/st-gallery.jpg" class="img-fluid"></a></div>
        </div>
      </div> -->

      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?
          // $gal = get_post_gallery( get_the_ID(), false );
          // var_dump(get_the_ID());
        ?>
        <article class="inner-page">
          <? the_content(); ?>
        </article>
      <? endwhile; else: ?>
        <h1>Страница не найдена</h1>
      <? endif; ?>
    </div>

    <div class="col-lg-3 order-first order-sm-last">
      <div class="sidebar">
        <div class="sidebar-title">Пространства клуба</div>
        <?php wp_nav_menu(array(
          'theme_location' => 'space_menu',
          'menu_class' => 'sidebar-menu',
          'container' => false
        )); ?>
        <a href="/poetazhnyj-plan/" class="btn-blue">Поэтажный план</a>
      </div>
    </div>

  </div>
</div>



<?php get_footer(); ?>
