<?php get_header(); ?>

<div class="container page-wrapper">
  <div class="row">

    <div class="col-12">
      <h1 class="page-title"><? the_title(); ?></h1>
    </div>

    <div class="col-lg-9">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="inner-page">
          <? the_content(); ?>
        </article>
      <? endwhile; else: ?>
        <h1>Страница не найдена</h1>
      <? endif; ?>
    </div>

    <div class="col-lg-3 d-none d-sm-block">
      <div class="sidebar text-center">
        <? get_template_part('tpl/sidebar-info'); ?>
      </div>
    </div>

  </div>
</div>



<?php get_footer(); ?>
