<?
/*
    Template Name: Услуги
    Template Post Type: page
*/

/**
 * Услуги (service-page.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<?php get_header(); ?>

<a href="/club-space/teams/" class="btn-green btn-float">Наши тренеры</a>

<div class="landing-block service-block-1">
  <div class="container">
    <div class="row">
      <div class="col-12 d-block d-sm-none">
        <h1 class="page-title"><? the_title(); ?></h1>
      </div>
      <div class="col">
        <div class="list-h">Персональные тренировки</div>
        <ul class="sub-list">
          <li>Персональные тренировки</li>
          <li>Сплит-тренировки</li>
        </ul>
        <a href="/uslugi/personalnye-trenirovki/" class="btn-b-white">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<div class="landing-block service-block-2">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-green">Индивидуальные программы</div>
        <ul class="sub-list">
          <li><a href="#">Составление программ тренировок</a></li>
          <li><a href="#">Составление программ питания</a></li>
        </ul>
        <a href="/uslugi/individualnye-programmy/" class="btn-b-green">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<div class="landing-block service-block-3">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-white">Красота<br/> и здоровье</div>
        <ul class="sub-list">
          <li><a href="#">Студия эстетики</a></li>
          <li><a href="#">Уход за телом</a></li>
          <li><a href="#">Уход за лицом</a></li>
          <li><a href="#">Массаж</a></li>
          <li><a href="#">Солярий</a></li>
        </ul>
        <a href="/uslugi/krasota-i-zdorove/" class="btn-b-white">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<div class="landing-block service-block-4">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-green">Функицональная диагностика</div>
        <ul class="sub-list">
          <li><a href="#">Определение допустимых нагрузок</a></li>
          <li><a href="#">Определение интенсивности тренировок</a></li>
          <li><a href="#">Подбор программы питания</a></li>
          <li><a href="#">Требуемые косметические процедуры</a></li>
        </ul>
        <a href="/uslugi/funkcionalnaya-trenirovka/" class="btn-b-green">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<div class="landing-block service-block-5">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-white">Секция<br/>для детей</div>
        <ul class="sub-list sub-list-white">
          <li><a href="#">Школа плавания</a></li>
          <li><a href="#">Школа фехтования</a></li>
          <li><a href="#">Школа танцев</a></li>
          <li><a href="#">Единоборства</a></li>
          <li><a href="#">Большой теннис</a></li>
        </ul>
        <a href="/uslugi/sekciya-dlya-detej/" class="btn-b-white">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<div class="landing-block service-block-6">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-blue">Будущим<br/>мамам</div>
        <ul class="sub-list">
          <li><a href="#">Aqua Prenatal</a></li>
          <li><a href="#">Yoga Prenatal</a></li>
          <li><a href="#">Занятия с персональным тренером</a></li>
        </ul>
        <a href="/uslugi/budushhim-mamam/" class="btn-b-blue">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<div class="landing-block service-block-7">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-white">Студии<br/>и фан-клубы</div>
        <ul class="sub-list">
          <li><a href="#">Занятия в кругу знакомых с тренером клуба</a></li>
          <li><a href="#">Группы по интересам</a></li>
        </ul>
        <a href="/uslugi/studii-i-fan-kluby/" class="btn-b-white">Подробнее</a>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
