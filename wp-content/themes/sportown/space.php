<?
/*
    Template Name: Пространство клуба
    Template Post Type: page
*/

/**
 * Пространство клуба (space.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<?php get_header(); ?>

<div class="scroll-wrapper">
<a href="/poetazhnyj-plan/" class="btn-green btn-float">Поэтажный план</a>
<div class="landing-block space-block-1">
  <div class="container">
    <div class="row">
      <div class="col-12 d-block d-sm-none">
        <h1 class="page-title text-white"><? the_title(); ?></h1>
      </div>
      <div class="col">
        <div class="list-h text-white">Аква-зона</div>
        <ul class="sub-list sub-list-white">
          <li>Бассеин 25 метров</li>
          <li>Сауна</li>
          <li>Хаммам</li>
        </ul>
        <a href="/club-space/akva-zona/" class="btn-b-blue text-white">Подробнее</a>
      </div>
    </div>
  </div>
</div>
<div class="landing-block space-block-2">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-blue">Тренажерный<br/>зал</div>
        <ul class="sub-list">
          <li><a href="/trainers/silovye-trenazhyory/">Cиловые тренажеры</a></li>
          <li><a href="/trainers/kardio-trenazhyory/">Кардио-тренажеры</a></li>
          <li><a href="/trainers/funkcionalnye-trenazhyory/">Функциональные тренажеры</a></li>
        </ul>
        <a href="/club-space/trenazhyornyj-zal/" class="btn-b-blue">Подробнее</a>
      </div>
    </div>
  </div>
</div>
<div class="landing-block space-block-3">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-white">На свежем<br/>воздухе</div>
        <ul class="sub-list sub-list-white">
          <li><a href="#">Большой теннис</a></li>
          <li><a href="#">Мини-футбол</a></li>
          <li><a href="#">Баскетбол</a></li>
        </ul>
        <a href="/club-space/na-svezhem-vozduxe/" class="btn-b-white text-white">Подробнее</a>
      </div>
    </div>
  </div>
</div>
<div class="landing-block space-block-4">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-blue">Детский<br/>клуб</div>
        <ul class="sub-list">
          <li><a href="#">Занятия в бассеине</a></li>
          <li><a href="#">Специальный зал для тренировок с детьми</a></li>
          <li><a href="#">Спортивные детские праздники</a></li>
        </ul>
        <a href="/club-space/detskij-klub/" class="btn-b-blue">Подробнее</a>
      </div>
    </div>
  </div>
</div>
<div class="landing-block space-block-5">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h">Зал<br/> единоборств</div>
        <ul class="sub-list">
          <li><a href="#">Зал единоборств</a></li>
        </ul>
        <a href="/club-space/zal-edinoborstv/" class="btn-b-white">Подробнее</a>
      </div>
    </div>
  </div>
</div>
<div class="landing-block space-block-6">
  <div class="container">
    <div class="row">
      <div class="col">
        <div class="list-h text-white">Настольный<br/>теннис</div>
        <a href="/club-space/nastolnyj-tennis/" class="btn-b-white text-white mt-4">Подробнее</a>
      </div>
    </div>
  </div>
</div>
</div>

<?php get_footer(); ?>
