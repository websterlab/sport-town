<!DOCTYPE html>
<html lang="ru-RU">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?php bloginfo('name') ?> <? wp_title();?></title>

	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<!-- jQuery End -->

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- Bootstrap End-->

	<!-- Stylesheet for this theme -->
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.theme.default.min.css">
	<!-- Stylesheet for this theme End -->

  <?php wp_head(); ?>
</head>

<body>

<nav class="navbar navbar-expand-lg fixed-top">
  <div class="container">
		<div class="navbar-mobile">
			<a href="/" class="navbar-brand"><img src="<?php bloginfo('template_url'); ?>/img/logo-small.svg" alt=""></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarMain" aria-controls="navbarMain" aria-expanded="false" aria-label="Toggle navigation"></button>
		</div>

		<div class="header-menu">
	    <div class="collapse navbar-collapse" id="navbarMain">
	      <?php wp_nav_menu([
	                        'theme_location' => 'header_menu',
	                        'menu_class'     => 'navbar-nav',
	                        'container'      => false
													]);
				?>
	      <?php wp_nav_menu([
	                        'theme_location' => 'footer_menu',
	                        'menu_class'     => 'navbar-nav mobile-menu',
	                        'container'      => false
													]);
				?>
				<div class="header-phone">
					<div class="phone"><a href="/contacts/">+7 (495) 780-88-86</a></div>
				</div>
	    </div>

		</div>
  </div>
</nav>
