<?
/*
    Template Name: Услуги (внутренняя)
    Template Post Type: page
*/

/**
 * Услуги (внутренняя) (inner-page-service.php)
 * @package WordPress
 * @subpackage sportown
*/
?>
<?php get_header(); ?>

<? get_template_part('tpl/breadcrumbs'); ?>

<div class="container page-wrapper">
  <div class="row">

    <div class="col-12 d-none d-sm-block">
      <h1 class="page-title"><? the_title(); ?></h1>
    </div>

    <div class="col-lg-9">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="inner-page">
          <? the_content(); ?>
        </article>
      <? endwhile; else: ?>
        <h1>Страница не найдена</h1>
      <? endif; ?>
    </div>

    <div class="col-lg-3 order-first order-sm-last">
      <div class="sidebar">
        <div class="sidebar-title">Услуги клуба</div>
        <?php wp_nav_menu(array(
          'theme_location' => 'service_menu',
          'menu_class' => 'sidebar-menu',
          'container' => false
        )); ?>
        <a href="/teams/" class="btn-blue">Наши тренеры</a>
      </div>
    </div>

  </div>
</div>



<?php get_footer(); ?>
