<?
/**
 * Тренажерные зоны (taxonomy-coaching_zone.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<? get_header(); ?>

<? get_template_part('tpl/breadcrumbs'); ?>

<div class="container page-wrapper">
  <div class="row">

    <div class="col-12 d-none d-sm-block">
      <h1 class="page-title"><? single_cat_title(); ?></h1>
    </div>

    <div class="col-md-9">
      <div class="row">
        <? if (have_posts()) : while (have_posts()) : the_post(); ?>
          <div class="col-lg-4 col-6 teams">
            <a href="<? the_permalink(); ?>" class="stretched-link"></a>
            <div class="teams-image">
              <? $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb'); ?>
              <img src="<?=$thumbnail_attributes[0];?>" class="img-fluid">
            </div>
            <h3 class="name"><? the_title(); ?></h3>
            <div class="master"><? the_field('sub_title', get_the_ID()); ?></div>
            <p class="stage">Тренерский стаж - <? the_field('coaching_staff', get_the_ID()); ?></p>
          </div>
        <? endwhile; else: ?>
          <div class="col">Тренеров не найдено</div>
        <? endif; ?>
      </div>
    </div>

    <div class="col-md-3 sidebar order-first order-sm-last">
      <div class="sidebar-title">Тренерские зоны</div>
      <?php wp_nav_menu(array(
        'theme_location' => 'teams_menu',
        'menu_class' => 'sidebar-menu',
        'container' => false
      )); ?>
      <a href="/club-space/" class="btn-blue">Пространство клуба</a>
    </div>

  </div>
</div>

<? get_footer(); ?>
