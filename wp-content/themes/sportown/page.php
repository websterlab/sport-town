<?php get_header(); ?>

<? get_template_part('tpl/breadcrumbs'); ?>

<div class="container mb-5">
  <div class="row">

    <div class="col-12">
      <h1 class="page-title"><? the_title(); ?></h1>
    </div>

    <div class="col-lg-12">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="inner-page">
          <? the_content(); ?>
        </article>
      <? endwhile; else: ?>
        <h1>Страница не найдена</h1>
      <? endif; ?>
    </div>

  </div>
</div>



<?php get_footer(); ?>
