<?php get_header(); ?>

<div class="homepage">

  <div class="home-block-1">

    <div class="container">
      <div class="row d-none d-sm-block">
        <div class="col">
          <a href="/" class="home-logo"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg"></a>
        </div>
      </div>
      <div class="row home-block-promo">
        <div class="col-lg-6 ml-auto text-sm-right">
          <div class="sticker-blue">Акции sportown</div>
          <h1>Вторая карта в подарок</h1>
          <p class="ml-auto">АКЦИЯ ДЕЙСТВИТЕЛЬНА ДО 10 АВГУСТА</p>
          <a href="/promotion/" class="btn-b-blue text-white">Подробнее</a>
        </div>
      </div>
      <div class="row block-2">
        <div class="col-lg-4">
          <div class="sticker-green">О клубе</div>
          <div class="home-text">Крупнейший фитнес-клуб на востоке Москвы, в котором есть все для того, чтобы обрести гармонию души и тела. На площади 5000 квадратных метров представлены разнообразные фитнес и бьюти программы для всех членов семьи.</div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="line-white"></div>
        </div>
      </div>
      <div class="row home-block-promo home-aqua">
        <div class="col-lg-6 ml-auto text-sm-right">
          <div class="sticker-green">Пространство клуба</div>
          <h1>Аква-зона</h1>
          <p class="ml-auto">Аква зона фитнес-клуба Sportown включает в себя большой бассейн площадью 25 метров, а также замечательный банных комплекс. Сауна и хамам – станут приятным завершением рабочего дня или восстановления после тренировки.</p>
          <a href="#" class="btn-b-white text-white">Подробнее</a>
        </div>
      </div>
      <div class="row sub-block-3 text-center">
        <div class="col-md-4">
          <div class="num-text">5000</div>
          <div class="num-desc">квадратных метров<br/>для тренировок</div>
        </div>
        <div class="col-md-4">
          <div class="num-text">345</div>
          <div class="num-desc">тренажеров от лучших<br/>производителей</div>
        </div>
        <div class="col-md-4">
          <div class="num-text">5400</div>
          <div class="num-desc">квадратных метров<br/>для занятий на улице</div>
        </div>
      </div>
    </div>

  </div>

  <div class="home-block-space">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 home-slide-item">
          <a href="/club-space/na-svezhem-vozduxe/" class="stretched-link"></a>
          <div class="home-slide-img">
            <img src="<?php bloginfo('template_url'); ?>/img/carousel-space/space2.jpg" class="img-fluid">
          </div>
          <div class="home-slide-desc">
            <div class="home-slide-title">На свежем воздухе</div>
            <div class="home-slide-subtitle">большой теннис, мини-футбол, баскетбол</div>
          </div>
        </div>
        <div class="col-lg-6 home-slide-item">
          <a href="/club-space/trenazhyornyj-zal/" class="stretched-link"></a>
          <div class="home-slide-img">
            <img src="<?php bloginfo('template_url'); ?>/img/carousel-space/space1.jpg" class="img-fluid">
          </div>
          <div class="home-slide-desc">
            <div class="home-slide-title">Тренажерный зал</div>
            <div class="home-slide-subtitle">силовые тренажеры, кардио-тренажеры, велодорожки</div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <a href="/club-space/" class="home-link">Все пространства</a>
        </div>
      </div>
    </div>
  </div>

  <?
    //Текущие события
    $current_event = new WP_Query([
      'post_type' => 'event',
      'posts_per_page' => '-1',
      'meta_query' => [
        'relation' => 'AND',
        [
          'key' => 'event_date_end',
          'value' => date('Y-m-d'),
          'type' => 'DATE',
          'compare' => '>='
        ]
      ]
    ]);
  ?>
  <div class="home-block-event">
    <div class="container">
      <div class="row">
        <div class="col-lg-3">
          <div class="event-archive">Ближайшие мероприятия</div>
        </div>
        <div class="col-lg-8 ml-auto">
          <? if($current_event->have_posts()): while($current_event->have_posts()): $current_event->the_post(); ?>
          <div class="event-archive-item">
            <div class="event-date"><? the_field('event_date_start'); ?> <span>&mdash;</span> <? the_field('event_date_end'); ?></div>
            <div class="event"><a href="/events/"><? the_title(); ?></a></div>
          </div>
          <? wp_reset_query(); ?>
          <? endwhile; endif; ?>
          <a href="/events/" class="home-link">Все мероприятия</a>
        </div>
      </div>
    </div>
  </div>

  <div class="home-block-service">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="sticker-blue">Услуги клуба</div>
        </div>
      </div>
      <div class="row block-service">
        <div class="col-lg-6 home-slide-item">
          <a href="/uslugi/personalnye-trenirovki/" class="stretched-link"></a>
          <div class="home-slide-img">
            <img src="<?php bloginfo('template_url'); ?>/img/carousel-service/img3.jpg" class="img-fluid">
          </div>
          <div class="home-slide-desc">
            <div class="home-slide-title">Персональные тренировки</div>
            <div class="home-slide-subtitle">ПЕРСОНАЛЬНЫЕ ТРЕНИРОВКИ, СПЛИТ-ТРЕНИРОВКИ</div>
          </div>
        </div>
        <div class="col-lg-6 home-slide-item">
          <a href="/uslugi/krasota-i-zdorove/" class="stretched-link"></a>
          <div class="home-slide-img">
            <img src="<?php bloginfo('template_url'); ?>/img/carousel-service/img1.jpg" class="img-fluid">
          </div>
          <div class="home-slide-desc">
            <div class="home-slide-title">Красота и здоровье</div>
            <div class="home-slide-subtitle">студия эстетики, уход за телом, уход за лицом, солярий</div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <a href="/usligi/" class="home-link">Все услуги</a>
        </div>
      </div>
    </div>
  </div>

  <div class="home-block-card">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="title">Клубная карта</div>
          <div class="text">Наши менеджеры с радостью подберут для вас вариант клубного членства, который подойдет под ваши потребности. В каждую клубную карту входит посещение всех клубных зон и возможность приобретения дополнительных услуг.</div>
          <a href="/club-card/" class="btn-b-white">Подробнее</a>
        </div>
      </div>
    </div>
  </div>


</div>

<?php get_footer(); ?>
