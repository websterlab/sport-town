<?
/*
    Template Name: Все тренеры
    Template Post Type: page
*/

/**
 * Команда (page-teams.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<? get_header(); ?>

<? get_template_part('tpl/breadcrumbs'); ?>

<?
  $query = new WP_Query([
    'post_type' => 'teams',
    'posts_per_page' => '-1'
  ]);
?>

<div class="container page-wrapper">
  <div class="row">

    <div class="col-12 d-none d-sm-block">
      <h1 class="page-title"><? the_title(); ?></h1>
    </div>

    <div class="col-md-9">
      <div class="row">
        <? if($query->have_posts()): while($query->have_posts()): $query->the_post(); ?>
          <div class="col-lg-4 col-6 teams">
            <a href="<? the_permalink(); ?>" class="stretched-link modal-link"></a>
            <div class="teams-image">
              <? $thumbnail_attributes = wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumb'); ?>
              <img src="<?=$thumbnail_attributes[0];?>" class="img-fluid">
            </div>
            <h3 class="name"><? the_title(); ?></h3>
            <div class="master"><? the_field('sub_title', get_the_ID()); ?></div>
            <p class="stage">Тренерский стаж - <? the_field('coaching_staff', get_the_ID()); ?></p>
          </div>
          <? wp_reset_query(); ?>
        <? endwhile; else: ?>
          <pre>Записей не найдено</pre>
        <? endif; ?>
      </div>
    </div>

    <div class="col-md-3 order-first order-sm-last">
      <div class="sidebar">
        <div class="sidebar-title">Тренерские зоны</div>
        <?php wp_nav_menu(array(
          'theme_location' => 'teams_menu',
          'menu_class' => 'sidebar-menu',
          'container' => false
        )); ?>
        <!-- <ul class="sidebar-menu">
          <?
            $args = array(
             'taxonomy' => 'coaching_zone',
             'orderby' => 'ID',
             'hide_empty' => 0,
             'title_li' => '',
             );
             wp_list_categories($args);
          ?>
        </ul> -->
        <a href="/club-space/" class="btn-blue">Пространство клуба</a>
      </div>
    </div>

  </div>
</div>

<? get_footer(); ?>
