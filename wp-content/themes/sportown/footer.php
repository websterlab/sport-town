<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col d-none d-lg-block">
        <?php wp_nav_menu(array(
          'theme_location' => 'footer_menu',
          'menu_class' => 'footer-menu',
          'container' => false
        )); ?>
      </div>
      <div class="col app">
        <span class="app-header">Приложения sportown</span>
        <div class="app-icons">
          <a href="https://apps.apple.com/app/id1273747943" target="_blank" rel="noreferrer"><img src="<?php bloginfo('template_url'); ?>/img/icons/appstore.svg" class="mr-2"/></a>
          <a href="https://play.google.com/store/apps/details?id=com.itrack.sportown592186" target="_blank" rel="noreferrer"><img src="<?php bloginfo('template_url'); ?>/img/icons/googleplay.svg"/></a>
        </div>
      </div>
    </div>
    <div class="row social">
      <div class="col">
        <a href="https://vk.com/sportown" target="_blank" rel="noreferrer"><img src="<?php bloginfo('template_url'); ?>/img/icons/vk.svg"/></a>
        <a href="https://www.facebook.com/sportown.fitness" target="_blank" rel="noreferrer"><img src="<?php bloginfo('template_url'); ?>/img/icons/fb.svg"/></a>
        <a href="https://www.instagram.com/sportown_fit/" target="_blank" rel="noreferrer"><img src="<?php bloginfo('template_url'); ?>/img/icons/instagram.svg"/></a>
      </div>
    </div>
    <div class="row site-copy">
      <div class="col-12"><a href="#">Политика обработки перснальных данных</a></div>
      <div class="col-12"><a href="/privacy-policy/">Политика конфиденциальности</a></div>
      <div class="col-12">© Sportown <?=date('Y');?></div>
    </div>
  </div>
</footer>

<!-- jQuery -->
<script>
  $("document").ready(function(){
    $(".sidebar-title").click(function(){
      $(".sidebar-menu").toggle();
      $(".sidebar-title").toggleClass("changed");
    });

    // Floor map
    $(".floor-sidebar li").click(function(){
      $(this).siblings('li').removeClass('active');
      $(this).addClass('active');
      var floor = $(this).data('floor');
      switch (floor) {
        case 'floor1':
          $('.floor-map-item').not('#'+floor).hide();
          $("#"+floor).show();
          break;
        case 'floor2':
          $('.floor-map-item').not('#'+floor).hide();
          $("#"+floor).show();
          break;
        case 'floor3':
          $('.floor-map-item').not('#'+floor).hide();
          $("#"+floor).show();
          break;
        default:
          $("#"+floor).show();
      }
    });

  });
</script>
<!-- jQuery End -->

<!-- Bootstrap -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<!-- Bootstrap End -->

<!-- Owl carousel -->
<!-- <script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
<script>
  $(document).ready(function(){
    $(".owl-carousel").owlCarousel({
      margin: 60,
      loop: true,
      autoWidth: true,
      items: 4,
      dots: false
    });
  });
</script> -->
<!-- Owl carousel End -->

<?php wp_footer(); ?>

</body>
</html>
