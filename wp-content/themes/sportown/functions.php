<?php

/*Поддержка темы*/
add_theme_support('post-thumbnails'); // поддержка миниатюр
/*End*/

/*Регистрируем меню*/
add_action('after_setup_theme', function(){
	register_nav_menus( array(
		'header_menu' => 'Меню в шапке',
		'space_menu' => 'Пространство клуба',
		'service_menu' => 'Услуги клуба',
		'teams_menu' => 'Тренерские зоны',
		'trainer_menu' => 'Тренажёры',
		'footer_menu' => 'Меню в подвале'
	) );
});
/*End*/

/*Регистрируем виджеты*/
// if (function_exists('register_sidebar')){
// 	register_sidebar([
// 		'name' => 'Footer',
// 		'id' => 'footer-sidebar',
// 		'description' => 'Эти виджеты будут показаны в нижней части сайта',
// 		'before_widget' => '<div class="footer-widget col">',
// 		'after_widget' => '</div>',
// 		'before_title' => '<h4>',
// 		'after_title' => '</h4>',
// 	]);
// }
/*End*/

/*Добавляем класс пунктам меню li*/
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    $classes[] = 'nav-item';
    return $classes;
}
/*End*/

/*Добавляем класс ссылкам в меню*/
add_filter('nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4);
function filter_nav_menu_link_attributes($atts, $item, $args, $depth){
  $atts['class'] .= 'nav-link';
  return $atts;
}
/*End*/

/* Breadcrumbs */
function the_breadcrumb(){
	// получаем номер текущей страницы
	$pageNum = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$separator = '<span>&sol;</span>';

	// если главная страница сайта
	if (is_front_page()){

		if ($pageNum > 1){
			echo '<a href="' . site_url() . '">Главная</a>' . $separator . $pageNum . '-я страница';
		} else {
			echo 'Вы находитесь на главной странице';
		}

	} else { // не главная

		//echo '<a href="' . site_url() . '">Главная</a>' . $separator;
		if (is_single()){ // записи
			$postType = get_queried_object();
			if (strcasecmp($postType->post_type, 'TRAINER')==0){
				echo "<a href='/club-space/trenazhyory/'>Все тренажёры</a>";
			}elseif(strcasecmp($postType->post_type, 'TEAMS')==0){
				echo "<a href='/club-space/teams/'>Все тренеры</a>";
			}else{
				the_category(', ');
			}
			echo $separator; the_title();
		} elseif (is_page()){ // страницы WordPress

			global $post;
			// если у текущей страницы существует родительская
			if ($post->post_parent){

				$parent_id  = $post->post_parent; // присвоим в переменную
				$breadcrumbs = array();

				while ($parent_id){
					$page = get_page( $parent_id );
					$breadcrumbs[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
					$parent_id = $page->post_parent;
				}

				echo join( $separator, array_reverse( $breadcrumbs ) ) . $separator;

			}
			the_title();

		} elseif (is_category()){

			single_cat_title();

		} elseif (is_tag()){

			single_tag_title();

		} elseif (is_day()){ // архивы (по дням)

			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
			echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a>' . $separator;
			echo get_the_time('d');

		} elseif (is_month()){ // архивы (по месяцам)

			echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
			echo get_the_time('F');

		} elseif (is_year()){ // архивы (по годам)

			echo get_the_time('Y');

		} elseif (is_author()){ // архивы по авторам

			global $author;
			$userdata = get_userdata($author);
			echo 'Опубликовал(а) ' . $userdata->display_name;

		} elseif (is_404()){ // если страницы не существует

			echo 'Ошибка 404';

		} elseif (is_singular($post_type_name)){

			$post_terms = get_the_terms( get_the_ID(), $taxonomy_name );

			if (!empty( $post_terms[0]->term_id)){
				echo get_term_parents_list( $post_terms->term_id, $taxonomy_name, array( 'separator' => $separator ) ) . $separator;
			}
			the_title();

		} elseif (is_tax($taxonomy_name)){

			$current_term = get_queried_object();
			$current_tax = $current_term->taxonomy;
			// если родительский элемент таксономии существует
			if ($current_term->parent ) {
				echo get_term_parents_list( $current_term->parent, $taxonomy_name, array( 'separator' => $separator ) ) . $separator;
			}

			if (strcasecmp($current_tax, 'COACHING_ZONE')==0 || strcasecmp($current_tax, 'TRAINERS')==0) {
				echo '<a href="/club-space/">Пространство клуба</a>'. $separator.'<a href="/club-space/teams/">Все тренеры</a>'. $separator;
			}

			single_term_title();
		}

		if ($pageNum > 1) { // номер текущей страницы
			echo ' ('.$pageNum.'-я страница)';
		}

	}

}
/* End */

// Post gallery
// function gallery_slider($output, $attr) {
// 	$ids = explode(',', $attr['ids']);
// 	$images = get_posts(array(
// 		'include'        => $ids,
// 		'post_status'    => 'inherit',
// 		'post_type'      => 'attachment',
// 		'post_mime_type' => 'image',
// 		'orderby' => 'post__in',
// 	));
// 	if ($images) {
// 		$output = gallery_slider_template($images);
// 		return $output;
// 	}
// }
// add_filter('post_gallery', 'gallery_slider', 10, 2);
// Post gallery End

/* Post Type: Команда */
function cptui_register_my_cpts_teams() {

	$labels = [
		"name" => __( "Команда", "custom-post-type-ui" ),
		"singular_name" => __( "Специалист", "custom-post-type-ui" ),
		"all_items" => __( "Все специалисты", "custom-post-type-ui" ),
		"add_new" => __( "Новый специалист", "custom-post-type-ui" ),
		"not_found" => __( "Специалистов не найдено", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "Специалистов не найдено", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Команда", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "teams", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-groups",
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "teams", $args );
}

add_action( 'init', 'cptui_register_my_cpts_teams' );

function cptui_register_my_taxes_coaching_zone() {
	$labels = [
		"name" => __( "Тренерские зоны", "custom-post-type-ui" ),
		"singular_name" => __( "Тренерская зона", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Тренерские зоны", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'coaching_zone', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "coaching_zone",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
			];
	register_taxonomy( "coaching_zone", [ "teams" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_coaching_zone' );
/* End */


/* Post Type: Тренажёры */
function cptui_register_my_cpts_trainer() {
	$labels = [
		"name" => __( "Тренажёры", "custom-post-type-ui" ),
		"singular_name" => __( "Тренажёр", "custom-post-type-ui" ),
		"all_items" => __( "Все тренажёры", "custom-post-type-ui" ),
		"add_new" => __( "Добавить тренажёр", "custom-post-type-ui" ),
		"add_new_item" => __( "Добавить тренажёр", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Тренажёры", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "trainer", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-admin-settings",
		"supports" => [ "title", "editor", "thumbnail", "excerpt" ],
	];

	register_post_type( "trainer", $args );
}

add_action( 'init', 'cptui_register_my_cpts_trainer' );

function cptui_register_my_taxes_trainers() {
	$labels = [
		"name" => __( "Типы тренажёров", "custom-post-type-ui" ),
		"singular_name" => __( "Тип тренажёра", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Типы тренажёров", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'trainers', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "trainers",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
			];
	register_taxonomy( "trainers", [ "trainer" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_trainers' );
/* End */

/* Post Type: События */
function cptui_register_my_cpts_event() {
	$labels = [
		"name" => __( "События", "custom-post-type-ui" ),
		"singular_name" => __( "Событие", "custom-post-type-ui" ),
		"all_items" => __( "Все события", "custom-post-type-ui" ),
		"add_new" => __( "Добавить событие", "custom-post-type-ui" ),
		"add_new_item" => __( "Добавить событие", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "События", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "event", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-megaphone",
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "event", $args );
}

add_action( 'init', 'cptui_register_my_cpts_event' );
/* End */

/* Post Type: Акция */
function cptui_register_my_cpts_promo() {
	$labels = [
		"name" => __( "Акции", "custom-post-type-ui" ),
		"singular_name" => __( "Акция", "custom-post-type-ui" ),
		"all_items" => __( "Все акции", "custom-post-type-ui" ),
		"add_new" => __( "Добавить акцию", "custom-post-type-ui" ),
		"add_new_item" => __( "Добавить акцию", "custom-post-type-ui" ),
		"new_item" => __( "Добавить акцию", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Акции", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "promo", "with_front" => true ],
		"query_var" => true,
		"menu_icon" => "dashicons-tickets-alt",
		"supports" => [ "title", "editor", "thumbnail" ],
	];

	register_post_type( "promo", $args );
}

add_action( 'init', 'cptui_register_my_cpts_promo' );
/* End */

?>
