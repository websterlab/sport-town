<?
/*
    Template Name: Поэтажный план
    Template Post Type: page
*/

/**
 * Поэтажный план (floor-page.php)
 * @package WordPress
 * @subpackage sportown
*/
?>

<? get_header(); ?>

<div class="floor-page">
  <div class="container">
    <div class="row">
      <div class="col-lg-3">
        <ul class="floor-sidebar">
          <li data-floor="floor1" class="active">Первый этаж</li>
          <li data-floor="floor2">Второй этаж</li>
          <li data-floor="floor3">Зал настольного тенниса</li>
        </ul>
      </div>
      <div class="col-lg-9">
        <div class="floor-map">
          <div class="floor-map-item" id="floor1">
            <img src="<?php bloginfo('template_url'); ?>/img/floor-maps/floor1.svg" usemap="#floor-map-1">
            <map name="floor-map-1">
                <area target="" alt="Детский спортивный зал" title="Детский спортивный зал" href="/club-space/detskij-klub/" coords="71,56,192,56,193,108,256,109,256,134,70,135" shape="poly">
                <area target="" alt="Зал единоборств" title="Зал единоборств" href="/club-space/zal-edinoborstv/" coords="32,137,255,137,255,224,113,225,75,215,48,193,38,172" shape="poly">
                <area target="" alt="Бассейн" title="Бассейн" href="/club-space/akva-zona/" coords="480,56,855,250" shape="rect">
            </map>
          </div>
          <div class="floor-map-item" id="floor2">
            <img src="<?php bloginfo('template_url'); ?>/img/floor-maps/floor2.svg" usemap="#floor-map-2">
            <map name="floor-map-2">
              <area target="" alt="Спортивный зал 2" title="Спортивный зал 2" href="#" coords="36,57,182,132" shape="rect">
              <area target="" alt="Спортивный зал 1" title="Спортивный зал 1" href="#" coords="35,138,254,138,255,226,105,226,75,217,51,196,40,177" shape="poly">
              <area target="" alt="Спортивный зал 3" title="Спортивный зал 3" href="#" coords="294,508,409,507,408,579,322,579,301,564,295,536" shape="poly">
              <area target="" alt="Тренажёрный зал" title="Тренажёрный зал" href="/club-space/trenazhyornyj-zal/" coords="295,58,479,58,479,579,412,579,412,503,295,504" shape="poly">
              <area target="" alt="Зал для индивидуальных тренировок" title="#" href="Зал для индивидуальных тренировок" coords="184,57,254,111" shape="rect">
          </map>
          </div>
          <div class="floor-map-item" id="floor3">
            <img src="<?php bloginfo('template_url'); ?>/img/floor-maps/floor3.svg" usemap="#floor-map-3">
            <map name="floor-map-3">
              <area target="" alt="Зал настольного тенниса" title="Зал настольного тенниса" href="/club-space/nastolnyj-tennis/" coords="406,356,479,578" shape="rect">
            </map>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<? get_footer(); ?>
